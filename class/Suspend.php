<?php
//Setting date dari UTC ke UTC+7 / Jakarta
date_default_timezone_set('Asia/Jakarta');

include_once('DB.php');

class Suspend extends DB
{
    public function getByUserId($user_id)
    {
        $sql = "SELECT * FROM suspends WHERE user_id = '$user_id'";
        $result = $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);

        //Ambil data terakhir
        $last_suspend = $result[count($result) - 1];

        if($last_suspend){
            return $last_suspend;
        }
        else{
            return "Gagal mengambil data";
        }
    }

    public function store()
    {
        //ambil data input dari form suspend
        $user_id = $_POST['user_id'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        //query untuk suspend si user
        $sql = "INSERT INTO suspends (user_id, start_date, end_date) VALUES ('$user_id', '$start_date', '$end_date')";
        $store = $this->db->query($sql); //eksekusi

        //ubah status user dari active menjadi suspend
        $sql_2 = "UPDATE users SET status = 'suspend' WHERE user_id = '$user_id'";
        $update = $this->db->query($sql_2);

        if($store){
            //jika berhasil
            return 'Suspend berhasil';
        }
        else{
            //jika gagal
            return 'Suspend gagal';
        }
    }

    public function update()
    {
        //ambil data input dari form suspend
        $user_id = $_POST['user_id'];
        $suspend_id = $_POST['suspend_id'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        $date_now = date('Y-m-d');

        //query untuk suspend si user
        $sql = "UPDATE suspends SET start_date = '$start_date', end_date = '$end_date' WHERE suspend_id = $suspend_id";
        $update = $this->db->query($sql); //eksekusi

        //ubah status user dari suspend menjadi active jika end date < date now
        if($end_date < $date_now){
            $sql_2 = "UPDATE users SET status = 'active' WHERE user_id = '$user_id'";
            $update_2 = $this->db->query($sql_2);
        }

        if($update){
            //jika berhasil
            return 'Update Suspend berhasil';
        }
        else{
            //jika gagal
            return 'Update Suspend gagal';
        }
    }
}

?>