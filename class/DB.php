<?php

class DB
{
    //configurasi database
    private $hostname = "localhost";
    private $username = "root";
    private $password = "";
    private $database = "4-fashion";

    protected $db;
    
    function __construct()
    {
        //koneksi ke database
        $this->db = mysqli_connect($this->hostname, $this->username, $this->password, $this->database);
    }
}

?>