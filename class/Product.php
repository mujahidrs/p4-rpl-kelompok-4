<?php

include_once('DB.php');

class Product extends DB
{
    public function getSellerById($seller_id)
    {
        $sql = "SELECT * FROM users WHERE user_id = $seller_id";
        $result = $this->db->query($sql)->fetch_assoc();

        if($result){
            return $result;
        }
        return "Gagal mengambil data";
    }

    public function index()
    {
        $sql = "SELECT * FROM products";
        $result = $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);

        return $result;
    }

    public function addToCart()
    {
        $customer_id = $_POST['customer_id'];
        $product_id = $_POST['product_id'];
        $quantity = $_POST['quantity'];
        $status = 'on cart';
        $created_at = date('Y-m-d H:i:s');

        $sql = "INSERT INTO transactions (customer_id, product_id, quantity, status, created_at) VALUES ('$customer_id', '$product_id', '$quantity', '$status', '$created_at')";
        $store = $this->db->query($sql);

        if($store){
            header('Location: index.php');
            return "Berhasil tambah ke keranjang";
        }
        return "Gagal tambah ke keranjang";
    }

    public function getById($product_id)
    {
        $sql = "SELECT * FROM products WHERE product_id = '$product_id'";
        $result = $this->db->query($sql)->fetch_assoc();

        return $result;
    }
}

?>