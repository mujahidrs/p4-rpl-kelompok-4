<?php
//Setting date dari UTC ke UTC+7 / Jakarta
date_default_timezone_set('Asia/Jakarta');

include_once('DB.php');

class Profile extends DB
{
    public function index()
    {
        $sql = "SELECT * FROM profiles";
        $profile = $this->db->query($sql)->fetch_assoc();

        if($profile){
            return $profile;
        }
        else {
            return "Gagal mengambil data";
        }
    }

    public function update()
    {
        //Ambil data dari form update
        $title = $_POST['title'];
        $slogan = $_POST['slogan'];
        $description = $_POST['description'];
        $updated_at = date('Y-m-d H:i:s');

        $targetDir = "../uploads/"; // Folder tujuan untuk menyimpan gambar yang diunggah
        $targetFile = $targetDir . basename($_FILES["logo"]["name"]); // Path lengkap file yang diunggah
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));

        $check = getimagesize($_FILES["logo"]["tmp_name"]);
        if ($check !== false) {
            // echo "File adalah gambar - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File bukan gambar.";
            $uploadOk = 0;
        }

        // Cek apakah file sudah ada di server
        // if (file_exists($targetFile)) {
        //     echo "Maaf, file tersebut sudah ada.";
        //     $uploadOk = 0;
        // }

        // Cek ukuran file
        if ($_FILES["logo"]["size"] > 500000) { // Ukuran maksimum file (dalam bytes)
            echo "Maaf, ukuran file terlalu besar.";
            $uploadOk = 0;
        }

        // Cek jenis file yang diizinkan (hanya gambar)
        $allowedExtensions = array("jpg", "jpeg", "png", "gif");
        if (!in_array($imageFileType, $allowedExtensions)) {
            echo "Maaf, hanya file JPG, JPEG, PNG, dan GIF yang diizinkan.";
            $uploadOk = 0;
        }

        // Jika semua pengecekan berhasil, lakukan proses upload
        if ($uploadOk == 1) {
            if (move_uploaded_file($_FILES["logo"]["tmp_name"], $targetFile)) {
                echo "File " . basename($_FILES["logo"]["name"]) . " berhasil diunggah.";
            } else {
                echo "Maaf, terjadi kesalahan saat mengunggah file.";
            }
        }

        //query untuk update
        $sql = "UPDATE profiles SET title = '$title', slogan = '$slogan', logo = '$targetFile', updated_at = '$updated_at', description = '$description'";
        $update = $this->db->query($sql); //eksekusi

        if($update){
            //jika berhasil
            return 'Update berhasil';
        }
        else{
            //jika gagal
            return 'Update gagal';
        }
    }
}

?>