<?php
date_default_timezone_set('Asia/Jakarta');

include_once('DB.php');

class User extends DB
{
    public function register()
    {
        $name = $_POST['name'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        // $password_enc = password_hash($password, PASSWORD_DEFAULT);
        $created_at = date('Y-m-d H:i:s');
        $role = $_POST['role'];

        $sql = "INSERT INTO users (name, username, password, role, created_at) VALUES ('$name', '$username', '$password', '$role', '$created_at')";
        $result = $this->db->query($sql);
        
        if($result) {
            return 'Registrasi berhasil';
        }
        else{
            return 'Registrasi gagal';
        }
    }

    public function update()
    {
        $user_id = $_GET['user_id'];

        $name = $_POST['name'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        // $password_enc = password_hash($password, PASSWORD_DEFAULT);
        $updated_at = date('Y-m-d H:i:s');

        $sql = "UPDATE users SET name = '$name', username = '$username', password = '$password', updated_at = '$updated_at' WHERE user_id = '$user_id'";
        $result = $this->db->query($sql);
        
        if($result) {
            return 'Update berhasil';
        }
        else{
            return 'Update gagal';
        }
    }

    public function login()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $sql = "SELECT * FROM users WHERE username = '$username'";
        $result = $this->db->query($sql)->fetch_assoc();
        //cek jika username ada
        if($result){
            //cek kecocokan password
            if($password == $result['password']){
                //berhasil login
                $last_login = date('Y-m-d H:i:s');
                $updated_at = date('Y-m-d H:i:s');
                $user_id = $result['user_id'];

                $sql_2 = "UPDATE users SET last_login = '$last_login', updated_at = '$updated_at' WHERE user_id = '$user_id'";
                $update = $this->db->query($sql_2);

                if($update){
                    $status = $result['status'];
                    $user_id = $result['user_id'];

                    if($status !== 'active'){
                        $sql_3 = "SELECT * FROM suspends WHERE user_id = '$user_id'";
                        $result_2 = $this->db->query($sql_3)->fetch_all(MYSQLI_ASSOC);

                        $last_suspend = strtotime($result_2[count($result_2) - 1]['end_date']);
                        $convert_last_suspend = date('Y-m-d',$last_suspend);
                        $date_now = date('Y-m-d');
                        
                        if($date_now > $convert_last_suspend) {
                            $sql_4 = "UPDATE users SET status = 'active' WHERE user_id = '$user_id'";
                            $update_2 = $this->db->query($sql_4);

                            if($update_2){
                                $this->redirect($result);
                            }
                            else{
                                return 'Gagal login';
                            }
                        }
                        else{
                            return 'Anda tidak diizinkan untuk login';
                        }
                    }
                    else{
                        $this->redirect($result);
                    }

                }
                else{
                    return 'Gagal login';
                }
            }
            else{
                return 'Password tidak cocok';
            }
        }
        else{
            return 'Username tidak ditemukan';
        }
    }

    public function logout()
    {
        session_destroy();
        header('Location: ../index.php');
    }

    private function redirect($result)
    {
        $_SESSION['username'] = $result['username'];
        $_SESSION['role'] = $result['role'];
        $role = $result['role'];

        header('Location: $role/index.php');

        return 'Berhasil login';
    }

    public function getByRole($role)
    {
        $sql = "SELECT * FROM users WHERE role = '$role'";
        $result = $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);

        if($result){
            return $result;
        }
        else{
            return 'Gagal mengambil data';
        }
    }

    public function getById($user_id)
    {
        $sql = "SELECT * FROM users WHERE user_id = '$user_id'";
        $result = $this->db->query($sql)->fetch_assoc();
        
        if($result){
            return $result;
        }
        else{
            return 'Gagal mengambil data';
        }
    }

    public function getByUsername($username)
    {
        $sql = "SELECT * FROM users WHERE username = '$username'";
        $result = $this->db->query($sql)->fetch_assoc();
        
        if($result){
            return $result;
        }
        else{
            return 'Gagal mengambil data';
        }
    }

    public function delete()
    {
        $user_id = $_POST['user_id'];

        $sql = "DELETE FROM users WHERE user_id = '$user_id'";
        $delete = $this->db->query($sql);

        if($delete){
            return "Berhasil menghapus user";
        }
        return "Gagal menghapus user";
    }
}

?>