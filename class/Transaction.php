<?php

include_once('DB.php');

class Transaction extends DB
{
    //Melihat keranjang saya berdasarkan customer_id
    public function my_cart($customer_id)
    {
        //query SQL untuk mengambil data dari tabel transactions berdasarkan customer_id dan status on cart
        $sql = "SELECT * FROM transactions WHERE customer_id = $customer_id AND status = 'on cart'";
        //eksekusi query dan mengeluarkan dalam bentuk objek array asosiatif
        $result = $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);

        //return hasil
        return $result;
    }

    //Menghapus produk dari keranjang
    public function deleteFromCart()
    {
        //mengambil cart_id dari form
        $cart_id = $_POST['cart_id'];

        //query SQL untuk menghapus transaksi yang id nya sesuai cart_id
        $sql = "DELETE FROM transactions WHERE transaction_id = '$cart_id'";
        //eksekusi query
        $delete = $this->db->query($sql);

        if($delete){
            //jika berhasil redirect ke index
            header('Location: index.php');

            return "Delete sukses";
        }
        return "Delete gagal";
    }

    //Mengambil total biaya pesanan dari keranjang berdasarkan customer_id
    public function getTotalCart($customer_id)
    {
        //query untuk mengambil data transaksi dari tabel transactions dengan relasi ke tabel products dan status on cart
        $sql = "SELECT * FROM transactions, products WHERE customer_id = '$customer_id' AND transactions.product_id = products.product_id AND status = 'on cart'";
        //eksekusi query dengan mengeluarkan semua datanya dalam bentuk objek array asosiatif
        $carts = $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);

        
        $total = 0;
        //hitung total harga dikali kuantitas
        foreach($carts as $cart){
            $total += $cart['price'] * $cart['quantity'];
        }

        //return hasil totalnya
        return $total;
    }

    //Mengambil total biaya pesanan dari checkout berdasarkan customer_id
    public function getTotalCheckout($customer_id)
    {
        //query untuk mengambil data transaksi dari tabel transactions dengan relasi ke tabel products dan status on cart
        $sql = "SELECT * FROM transactions, products WHERE customer_id = '$customer_id' AND transactions.product_id = products.product_id AND status = 'checkout'";
        //eksekusi query dengan mengeluarkan semua datanya dalam bentuk objek array asosiatif
        $carts = $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);

        
        $total = 0;
        //hitung total harga dikali kuantitas
        foreach($carts as $cart){
            $total += $cart['price'] * $cart['quantity'];
        }

        //return hasil totalnya
        return $total;
    }

    //Method ini dipanggil saat customer melakukan checkout dari keranjang
    public function checkout()
    {
        //Ambil customer_id dari form
        $customer_id = $_POST['customer_id'];
        //Generate invoice dengan perpaduan random number, customer_id dan tanggal waktu terkini
        $invoice_code = "INV_" . rand() . $customer_id . date('YmdHis');

        //query untuk mengupdate transaksi yg statusnya on cart untuk diberikan invoice code
        $sql = "UPDATE transactions SET invoice_code = '$invoice_code', status = 'checkout' WHERE status = 'on cart' AND customer_id = '$customer_id'";
        //eksekusi query
        $checkout = $this->db->query($sql);

        if($checkout){
            //redirect ke halaman invoice
            header("Location: invoice.php?code=$invoice_code");
        }
        else{
            return "Gagal melakukan checkout";
        }
    }

    //Mengambil transaksi berdasarkan invoice code
    public function getTransactionByInvoiceCode($invoice_code)
    {
        //query untuk mengambil data transaksi berdasarkan invoice code dengan relasi kepada tabel produk
        $sql = "SELECT * FROM transactions, products WHERE invoice_code = '$invoice_code' AND transactions.product_id = products.product_id";
        //eksekusi query dengan objek array asosiatif
        $transaction = $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);

        //return hasilnya
        return $transaction;
    }

    //Method ini dipanggil ketika customer mengonfirmasi telah melakukan pembayaran setelah checkout
    public function confirmPaid($invoice_code)
    {
        //query untuk mengupdate status transaksi dari checkout menjadi paid
        $sql = "UPDATE transactions SET status = 'paid' WHERE invoice_code = '$invoice_code' AND status = 'checkout'";
        //eksekusi query
        $confirm = $this->db->query($sql);

        if($confirm){
            //redirect ke halaman orders customer
            header("Location: orders.php");
        }
        else{
            return "Gagal konfirmasi pembayaran";
        }
    }

    //Mengambil data pesanan yang dikelompokkan berdasarkan invoice_code dan customer_id
    public function getOrder(){
        //query untuk mengambil data pesanan dengan mengelompokkannya berdasarkan invoice_code dan customer_id
        $sql = "SELECT customer_id, invoice_code FROM transactions GROUP BY customer_id, invoice_code";
        //eksekusi query
        $result = $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);

        //return hasilnya
        return $result;
    }

    //Mengambil data pesanan yang dikelompokkan berdasarkan invoice_code dan customer_id yang diambil berdasarkan status
    public function getOrderByStatus($status){
        //query untuk mengambil data pesanan dengan mengelompokkannya berdasarkan invoice_code dan customer_id yang diambil berdasarkan status
        $sql = "SELECT customer_id, invoice_code, status FROM transactions WHERE status = '$status' GROUP BY customer_id, invoice_code";
        //eksekusi query
        $result = $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);

        //return hasilnya
        return $result;
    }

    //Method ini dipanggil ketika admin mengonfirmasi telah menerima pembayaran dari customer
    public function approve()
    {
        //Ambil invoice_code dari form
        $invoice_code = $_POST['invoice_code'];

        //query untuk mengupdate transaksi dengan invoice_code yg diminta agar diubah statusnya dari paid menjadi approved
        $sql = "UPDATE transactions SET status = 'approved' WHERE invoice_code = '$invoice_code'";
        //eksekusi query
        $approve = $this->db->query($sql);

        if($approve){
            //redirect ke halaman orders admin
            header('Location: orders.php');
            return "Berhasil konfirmasi";
        }
        else{
            return "Gagal mengonfirmasi pembayaran";
        }
    }

    //Method ini dipanggil ketika seller mengonfirmasi sedang memproses pesanan
    public function process()
    {
        //Ambil invoice_code dari form
        $invoice_code = $_POST['invoice_code'];

        //query untuk mengupdate transaksi dengan invoice_code yg diminta agar diubah statusnya dari approved menjadi process
        $sql = "UPDATE transactions SET status = 'process' WHERE invoice_code = '$invoice_code'";
        //eksekusi query
        $process = $this->db->query($sql);

        if($process){
            //redirect ke halaman orders seller
            header('Location: orders.php');
            return "Berhasil diproses";
        }
        else{
            return "Gagal memproses pesanan";
        }
    }

    //Method ini dipanggil ketika seller mengonfirmasi sedang memproses pesanan
    public function send()
    {
        //Ambil invoice_code dari form
        $invoice_code = $_POST['invoice_code'];

        //query untuk mengupdate transaksi dengan invoice_code yg diminta agar diubah statusnya dari process menjadi sending
        $sql = "UPDATE transactions SET status = 'sending' WHERE invoice_code = '$invoice_code'";
        //eksekusi query
        $send = $this->db->query($sql);

        if($send){
            //redirect ke halaman orders seller
            header('Location: orders.php');
            return "Berhasil dikirim";
        }
        else{
            return "Gagal mengirim pesanan";
        }
    }

    //Mengambil status dari sebuah transaksi berdasarkan invoice
    public function getStatusByInvoice($invoice_code)
    {
        //query untuk mengambil status dari sebuah transaksi berdasarkan invoice
        $sql = "SELECT status FROM transactions WHERE invoice_code = '$invoice_code'";
        //eksekusi query
        $status = $this->db->query($sql)->fetch_assoc();

        //return status
        return $status;
    }
}

?>