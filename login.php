<?php
session_start();
include("class/User.php");

if(isset($_POST["submit"])){
    $user = new User;
    echo $user->login();
}

if(isset($_SESSION['username'])){
    $role = $_SESSION['role'];
    header("Location: $role/index.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <title>Login</title>
</head>
<body>
    <form method="POST" action="">
        <div>
            <label>Username</label>
            <input type="text" name="username" placeholder="Masukkan username">
        </div>
        <div>
            <label>Password</label>
            <input type="password" name="password" placeholder="Masukkan password">
        </div>
        <input type="submit" name="submit" value="Login">
    </form>
</body>
</html>