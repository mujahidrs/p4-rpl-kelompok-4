<?php
session_start();

include_once('restrict.php');  
include_once('../class/User.php');
include_once('../class/Suspend.php');
$user_id = $_GET['user_id'];

$user = new User;
$userdata = $user->getById($user_id);

$suspend = new Suspend;
$data_suspend = $suspend->getByUserId($user_id);

if(isset($_POST['submit'])){
    $update = $suspend->update();

    if($update){
        header("Location: customers.php");
    }
    else{
        echo $store;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Suspend</title>
</head>
<body>
    <form method="POST" action="">
        <div>
            <label>Username</label>
            <input type="text" name="username" value="<?= $userdata['username'] ?>" disabled>
            <input type="hidden" name="user_id" value="<?= $userdata['user_id'] ?>">
            <input type="hidden" name="suspend_id" value="<?= $data_suspend['suspend_id'] ?>">
        </div>

        <div>
            <label>Start Date</label>
            <input type="date" name="start_date" value="<?= $data_suspend['start_date'] ?>">
        </div>

        <div>
            <label>End Date</label>
            <input type="date" name="end_date" value="<?= $data_suspend['end_date'] ?>">
        </div>

        <input type="submit" name="submit" value="Update">
    </form>
</body>
</html>