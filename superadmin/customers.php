<?php
session_start();

include_once('restrict.php');  
include_once('../class/User.php');

$user = new User;
$customers = $user->getByRole('customer');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Customers</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Username</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($customers as $key => $customer): ?>
                <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $customer['name'] ?></td>
                    <td><?= $customer['username'] ?></td>
                    <td><?= $customer['status'] ?></td>
                    <td>
                        <?php if($customer['status'] === 'active') : ?>
                            <a href="suspend.php?user_id=<?= $customer['user_id'] ?>">Suspend</a>
                        <?php else : ?>
                            <a href="detail_suspend.php?user_id=<?= $customer['user_id'] ?>">Detail</a>
                        <?php endif ?>
                    <td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</body>
</html>