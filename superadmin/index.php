<?php
session_start();

include_once('restrict.php');  
if(!isset($_SESSION['username'])){
    header('Location: ../index.php?pesan=Anda belum login!');
}
include_once('../class/User.php');  

$username = $_SESSION['username'];

if(isset($_GET['submit'])){
    $user = new User;
    $user->logout();
}




?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    Welcome to Superadmin Dashboard

    username: <?= $username ?>

    <form method="GET">
        <input type="submit" value="Logout" name="submit">
    </form>

    <hr>

    <div><a href="profile.php">Company Profile</a></div>
    <div><a href="customers.php">Customers</a></div>
    <div><a href="admins/index.php">Admins</a></div>
</body>
</html>