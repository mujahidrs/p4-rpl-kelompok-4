<?php
session_start();

include_once('restrict.php');  
include_once('../class/Profile.php');

$profile = new Profile;
$company_profile = $profile->index();

if(isset($_POST['submit'])){
    echo $profile->update();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>
</head>
<body>
    <form method="POST" action="" enctype="multipart/form-data">
        <div>
            <label>Title</label>
            <input type="text" name="title" value="<?= $company_profile['title'] ?>">
        </div>
        <div>
            <label>Slogan</label>
            <input type="text" name="slogan" value="<?= $company_profile['slogan'] ?>">
        </div>
        <div>
            <label>Logo</label>
            <img src="<?= $company_profile['logo'] ?>" height="100">
            <input type="file" name="logo">
        </div>
        <div>
            <label>Description</label>
            <textarea name="description"><?= $company_profile['description'] ?></textarea>
        </div>
        <input type="submit" name="submit" value="Update">
    </form>
</body>
</html>