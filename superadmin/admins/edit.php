<?php
session_start();

include_once('../restrict.php');
include_once('../../class/User.php');

$user_id = $_GET['user_id'];
$user = new User;

if(isset($_POST['update'])){
    echo $user->update();
}

$data_user = $user->getById($user_id);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Admin</title>
</head>
<body>
    <form method="POST" action="">
        <div>
            <label>Name</label>
            <input type="text" name="name" value="<?= $data_user['name'] ?>">
        </div>
        <div>
            <label>Username</label>
            <input type="text" name="username" value="<?= $data_user['username'] ?>">
        </div>
        <div>
            <label>Password</label>
            <input type="text" name="password" value="<?= $data_user['password'] ?>">
        </div>

        <input type="submit" name="update" value="Update">
    </form>
</body>
</html>