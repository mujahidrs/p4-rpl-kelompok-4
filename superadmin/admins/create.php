<?php
session_start();

include_once('../restrict.php');
include_once('../../class/User.php');

if(isset($_POST['register'])){
    $user = new User;
    echo $user->register();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Admin</title>
</head>
<body>
    <form method="POST" action="">
        <div>
            <label>Name</label>
            <input type="text" name="name">
        </div>
        <div>
            <label>Username</label>
            <input type="text" name="username">
        </div>
        <div>
            <label>Password</label>
            <input type="password" name="password">
        </div>
        <input type="hidden" name="role" value="admin">

        <input type="submit" name="register" value="Submit">
    </form>
</body>
</html>