<?php
session_start();

include_once('../restrict.php');
include_once('../../class/User.php');

$user = new User;
$admins = $user->getByRole('admin');

if(isset($_POST['delete'])){
    echo $user->delete();
    header("Location: index.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admins</title>
</head>
<body>
    <a href="create.php">Tambah Admin</a>
    <table>
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Username</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($admins as $key => $admin): ?>
                <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $admin['name'] ?></td>
                    <td><?= $admin['username'] ?></td>
                    <td><?= $admin['status'] ?></td>
                    <td>
                        <?php if($admin['status'] === 'active') : ?>
                            <a href="../suspend.php?user_id=<?= $admin['user_id'] ?>">Suspend</a>
                        <?php else : ?>
                            <a href="detail_suspend.php?user_id=<?= $admin['user_id'] ?>">Detail</a>
                        <?php endif ?>
                        
                        <a href="edit.php?user_id=<?= $admin['user_id'] ?>">Edit</a>

                        <form method="POST" action="">
                            <input type="hidden" name="user_id" value="<?= $admin['user_id'] ?>">
                            <input type="submit" name="delete" value="Hapus">
                        </form>
                    <td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</body>
</html>