-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 11, 2023 at 03:26 PM
-- Server version: 8.0.30
-- PHP Version: 8.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `4-fashion`
--

-- --------------------------------------------------------

--
-- Table structure for table `discussions`
--

CREATE TABLE `discussions` (
  `discussion_id` int NOT NULL,
  `ticket_id` int NOT NULL,
  `message` text NOT NULL,
  `user_id` int NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `discussions`
--

INSERT INTO `discussions` (`discussion_id`, `ticket_id`, `message`, `user_id`, `created_at`) VALUES
(1, 2, 'Sebentar saya liat videonya dulu yaa. Sedang dalam pemeriksaan', 3, '2023-05-10 08:40:00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `price` int NOT NULL,
  `stock` int NOT NULL,
  `seller_id` int NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL,
  `category` varchar(50) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `image` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `name`, `description`, `price`, `stock`, `seller_id`, `created_at`, `updated_at`, `deleted_at`, `category`, `brand`, `image`) VALUES
(1, 'Seragam PPPK', 'Putih lengan pendek', 100000, 100, 3, '2023-05-10 06:48:49', '2023-05-10 06:34:55', '2023-05-10 06:48:49', 'Atasan', 'Brand 1', ''),
(2, 'Celaan Bahan', 'Hitam Panjang Slimfit', 150000, 100, 3, '2023-05-10 08:33:12', '2023-05-10 06:35:08', '2023-05-10 08:33:12', 'Bawahan', 'Brand 2', '');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `title` varchar(255) NOT NULL,
  `slogan` varchar(255) NOT NULL,
  `logo` text NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bank_name` varchar(20) NOT NULL,
  `account_name` varchar(30) NOT NULL,
  `account_number` char(20) NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`title`, `slogan`, `logo`, `description`, `bank_name`, `account_name`, `account_number`, `updated_at`) VALUES
('4 Fashion', 'Ayo belanja di 4 Fashion!', '../uploads/logo_atoms.png', '4 Fashion keren', 'BNI', '4 Fashion Sejahtera', '0312656276', '2023-05-11 20:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `suspends`
--

CREATE TABLE `suspends` (
  `suspend_id` int NOT NULL,
  `user_id` int NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `suspends`
--

INSERT INTO `suspends` (`suspend_id`, `user_id`, `start_date`, `end_date`) VALUES
(6, 7, '2023-05-02', '2023-05-09'),
(7, 7, '2023-05-02', '2023-05-09'),
(8, 7, '2023-05-08', '2023-05-10'),
(9, 7, '2023-05-09', '2023-05-10');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `ticket_id` int NOT NULL,
  `title` varchar(50) NOT NULL,
  `desc` text NOT NULL,
  `transaction_id` int NOT NULL,
  `status` enum('open','closed') NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`ticket_id`, `title`, `desc`, `transaction_id`, `status`, `created_at`) VALUES
(2, 'Barang Rusak', 'Celana robek dibagian lutut', 1, 'open', '2023-05-10 08:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `transaction_id` int NOT NULL,
  `customer_id` int NOT NULL,
  `product_id` int NOT NULL,
  `quantity` int NOT NULL,
  `invoice_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` enum('on cart','checkout','paid','approved','process','sending','received','canceled') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`transaction_id`, `customer_id`, `product_id`, `quantity`, `invoice_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, 2, 5, 'INV_001', 'sending', '2023-05-10 08:37:18', '2023-05-10 08:37:18'),
(18, 4, 1, 3, 'INV_167207241420230511135232', 'sending', '2023-05-11 13:51:31', NULL),
(20, 4, 2, 2, 'INV_167207241420230511135232', 'sending', '2023-05-11 13:52:03', NULL),
(24, 7, 1, 1, 'INV_1209033466720230511144920', 'sending', '2023-05-11 14:49:12', NULL),
(25, 7, 2, 1, 'INV_1209033466720230511144920', 'sending', '2023-05-11 14:49:13', NULL),
(26, 7, 1, 3, 'INV_1949990959720230511214952', 'sending', '2023-05-11 21:48:13', NULL),
(28, 7, 2, 1, 'INV_1949990959720230511214952', 'sending', '2023-05-11 21:48:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `role` enum('admin','superadmin','seller','customer') NOT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `phone` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` enum('active','suspend') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'active',
  `last_login` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `username`, `password`, `role`, `address`, `phone`, `status`, `last_login`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mujahid', 'mujahid', 'mujahid', 'superadmin', 'Jl. Dukuh', '08968273827', 'active', '2023-05-11 21:11:47', '2023-05-10 06:45:39', '2023-05-11 21:11:47', '2023-05-10 06:45:39'),
(2, 'Robbani', 'robbani', 'robbani', 'admin', 'Jl. Dukuh', '08968273827', 'active', '2023-05-11 14:58:55', '2023-05-10 06:46:29', '2023-05-11 14:58:55', '2023-05-10 06:46:29'),
(3, 'Sholahudin', 'sholahudin', 'sholahudin', 'seller', 'Jl. Dukuh', '08968273827', 'active', '2023-05-11 21:53:43', '2023-05-10 06:46:29', '2023-05-11 21:53:43', '2023-05-10 06:46:29'),
(4, 'Hamasah', 'hamasah', 'hamasah', 'customer', 'Jl. Dukuh', '08968273827', 'active', '2023-05-11 13:50:18', '2023-05-10 08:32:33', '2023-05-11 13:50:18', '2023-05-10 08:32:33'),
(7, 'Inanda', 'inanda', 'inanda', 'customer', 'Jl. Dukuh', '08968273827', 'active', '2023-05-11 21:47:49', '2023-05-10 06:52:28', '2023-05-11 21:47:49', '2023-05-10 06:52:28'),
(10, 'Muji Antoro', 'mujiantoro', 'mujiantoro', 'admin', NULL, NULL, 'active', '2023-05-11 21:52:01', '2023-05-11 21:39:51', '2023-05-11 21:52:01', NULL),
(11, 'Wirvan', 'wirvan', 'wirvan', 'admin', NULL, NULL, 'active', NULL, '2023-05-11 21:40:09', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `discussions`
--
ALTER TABLE `discussions`
  ADD PRIMARY KEY (`discussion_id`),
  ADD KEY `discussionibfk` (`ticket_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `suspends`
--
ALTER TABLE `suspends`
  ADD PRIMARY KEY (`suspend_id`),
  ADD KEY `suspendibfk` (`user_id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ticket_id`),
  ADD KEY `ticketibgfk` (`transaction_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `transactionibfk` (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `discussions`
--
ALTER TABLE `discussions`
  MODIFY `discussion_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `suspends`
--
ALTER TABLE `suspends`
  MODIFY `suspend_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ticket_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `transaction_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `discussions`
--
ALTER TABLE `discussions`
  ADD CONSTRAINT `discussionibfk` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`ticket_id`);

--
-- Constraints for table `suspends`
--
ALTER TABLE `suspends`
  ADD CONSTRAINT `suspendibfk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `ticketibgfk` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`transaction_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
