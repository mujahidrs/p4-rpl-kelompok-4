<?php
session_start();

include_once('class/Profile.php');
include_once('class/User.php');
include_once('class/Product.php');
include_once('class/Transaction.php');

if(isset($_GET['pesan'])){
    echo $_GET['pesan'];
}

$profile = new Profile;
$user = new User;
$product = new Product;
$transaction = new Transaction;

$company_profile = $profile->index();
$data_product = $product->index();

if(isset($_SESSION['username'])){
    $username = $_SESSION['username'];
    $data_user = $user->getByUsername($username);
    $data_keranjang = $transaction->my_cart($data_user['user_id']);
}

if(isset($_GET['submit'])){
    $user->logout();
}

if(isset($_POST['addToCart'])){
    if(isset($_SESSION['username'])){
        $product->addToCart();
    }
    else{
        header('Location: login.php');
    }
}


if(isset($_POST['delete_cart'])){
    $transaction->deleteFromCart();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <title>4 Fashion</title>
</head>
<body>
    <nav class="navbar bg-primary" data-bs-theme="dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="<?= $company_profile['logo'] ?>" alt="Logo" width="30" height="24" class="d-inline-block align-text-top">
            <?= $company_profile['title'] ?>
        </a>
        <div class="text-end">
        <?php if(!isset($username)) : ?>
        <a class="btn btn-success" href="login.php">Login</a>
    <?php else : ?>
        <span class="text-white">Welcome, <?= $data_user['name'] ?></span>
        <form method="GET">
            <button class="btn btn-danger" type="submit" name="submit">Logout</button>
        </form>
        </div>
    <?php endif ?>
    </div>
    </nav>
    <div class="card mb-3">
        <div class="card-body">
            <h5 class="card-title"><?= $company_profile['slogan'] ?></h5>
            <p class="card-text"><?= $company_profile['description'] ?></p>
        </div>
    </div>
    <div class="row container-fluid">
        <div class="col">
        <div class="card">
            <div class="card-header">
                <h3>Katalog Produk</h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Gambar</th>
                            <th>Harga</th>
                            <th>Stok</th>
                            <th>Penjual</th>
                            <th>Kategori</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data_product as $key => $p) : ?>
                            <tr>
                                <td><?= $key + 1 ?></td>
                                <td><?= $p['name'] ?></td>
                                <td><?= $p['image'] ?></td>
                                <td><?= $p['price'] ?></td>
                                <td><?= $p['stock'] ?></td>
                                <td><?= $product->getSellerById($p['seller_id'])['name'] ?></td>
                                <td><?= $p['category'] ?></td>
                                <td>
                                    <form method="POST" action="">
                                        <div class="input-group mb-3">
                                            <input type="number" class="form-control" name="quantity" min="1" max="<?= $p['stock'] ?>" value="1">
                                            <button type="submit" name="addToCart" class="btn btn-warning" type="button">Add to Cart</button>
                                        </div>
                                        <input class="form-control" type="hidden" name="customer_id" value="<?= $data_user["user_id"] ?>">
                                        <input class="form-control" type="hidden" name="product_id" value="<?= $p['product_id'] ?>">
                                    </form>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                </table>
                </div>
            </div>
        </div>
        <?php if(isset($_SESSION['username'])) : ?>
        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    <h3>Keranjang Saya</h3>
                </div>
                <div class="card-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Jumlah</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data_keranjang as $key => $cart) : ?>
                            <tr>
                                <td><?= $key + 1 ?></td>
                                <td><?= $product->getById($cart['product_id'])['name'] ?></td>
                                <td><?= $product->getById($cart['product_id'])['price'] ?></td>
                                <td><?= $cart['quantity'] ?></td>
                                <td>
                                    <form method="POST" action="">
                                        <input type="hidden" name="cart_id" value="<?= $cart["transaction_id"] ?>">
                                        <button class="btn btn-danger" type="submit" name="delete_cart">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            <?php endforeach ?>
                    </tbody>
                </table>
                </div>
                <div class="card-footer row">
                    <div class="col">
                        <h5>Total: <?= $transaction->getTotalCart($data_user['user_id']) ?></h5>
                    </div>
                    <div class="col text-end">
                        <a class="btn btn-primary" href="customer/checkout.php?id=<?= $data_user['user_id'] ?>">Checkout</a>
                    </div>
                </div>
            </div>
        </div>
        <?php endif ?>
    </div>

    
    
    

    
</body>
</html>