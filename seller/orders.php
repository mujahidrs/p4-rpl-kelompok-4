<?php
session_start();

include_once('restrict.php');

include_once('../class/User.php');
include_once('../class/Transaction.php');

$user = new User;
$transaction = new Transaction;
$data_orders = $transaction->getOrder();

if(isset($_POST['approve'])){
    echo $transaction->approve();
}

if(isset($_SESSION['username'])){
    $username = $_SESSION['username'];
    $data_user = $user->getByUsername($username);
}

if(isset($_POST['process'])){
    $transaction->process();
}

if(isset($_POST['send'])){
    $transaction->send();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pesanan Masuk</title>
</head>
<body>
<h3>Pesanan Masuk</h3>
<table>
    <thead>
            <tr>
                <th>No.</th>
                <th>Pembeli</th>
                <th>Alamat</th>
                <th>No. HP</th>
                <th>Invoice</th>
                <th>Status</th>
                <th>Total</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($data_orders as $key => $order) : ?>
                <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $user->getById($order['customer_id'])['name'] ?></td>
                    <td><?= $user->getById($order['customer_id'])['address'] ?></td>
                    <td><?= $user->getById($order['customer_id'])['phone'] ?></td>
                    <td><?= $order['invoice_code'] ?></td>
                    <td><?= $transaction->getStatusByInvoice($order['invoice_code'])['status'] ?></td>
                    <td>
                        <?php
                            $total = 0;
                            $ongkir = 9000;
                            foreach($transaction->getTransactionByInvoiceCode($order['invoice_code']) as $trx){
                                $total += $trx['price'] * $trx['quantity'];
                            }
                            echo $total + $ongkir;
                        ?>
                    </td>
                    <td>
                        <?php if($transaction->getStatusByInvoice($order['invoice_code'])['status'] === 'approved') : ?>
                            <form method="POST" action="">
                                <input type="hidden" name="invoice_code" value="<?= $order['invoice_code'] ?>">
                                <input type="submit" name="process" value="Proses Pesanan">
                            </form>
                        <?php endif ?>
                        <?php if($transaction->getStatusByInvoice($order['invoice_code'])['status'] === 'process') : ?>
                            <form method="POST" action="">
                                <input type="hidden" name="invoice_code" value="<?= $order['invoice_code'] ?>">
                                <input type="submit" name="send" value="Kirim Pesanan">
                            </form>
                        <?php endif ?>
                    </td>
                </tr>
                <?php endforeach ?>
        </thead>
    </table>

</body>
</html>