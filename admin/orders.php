<?php
session_start();

include_once('restrict.php');

include_once('../class/User.php');
include_once('../class/Transaction.php');

$user = new User;
$transaction = new Transaction;
$data_orders = $transaction->getOrderByStatus('paid');

if(isset($_POST['approve'])){
    echo $transaction->approve();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pesanan Masuk</title>
</head>
<body>
<table>
    <thead>
            <tr>
                <th>No.</th>
                <th>Pembeli</th>
                <th>Alamat</th>
                <th>No. HP</th>
                <th>Invoice</th>
                <th>Status</th>
                <th>Total</th>
                <th>Aksi</th>
            </tr>
            <?php foreach($data_orders as $key => $order) : ?>
                <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $user->getById($order['customer_id'])['name'] ?></td>
                    <td><?= $user->getById($order['customer_id'])['address'] ?></td>
                    <td><?= $user->getById($order['customer_id'])['phone'] ?></td>
                    <td><?= $order['invoice_code'] ?></td>
                    <td><?= $order['status'] ?></td>
                    <td>
                        <?php
                            $total = 0;
                            $ongkir = 9000;
                            foreach($transaction->getTransactionByInvoiceCode($order['invoice_code']) as $trx){
                                $total += $trx['price'] * $trx['quantity'];
                            }
                            echo $total + $ongkir;
                        ?>
                    </td>
                    <td>
                        <form method="POST" action="">
                            <input type="hidden" name="invoice_code" value="<?= $order['invoice_code'] ?>">
                            <input type="submit" name="approve" value="Konfirmasi Terima Pembayaran">
                        </form>
                    </td>
                </tr>
                <?php endforeach ?>
        </thead>
    </table>
</body>
</html>