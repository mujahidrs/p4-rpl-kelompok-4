<?php
session_start(); 

include_once('restrict.php');

include_once('../class/User.php');
include_once('../class/Product.php');
include_once('../class/Transaction.php');

$user = new User;
$product = new Product;
$transaction = new Transaction;

if(isset($_SESSION['username'])){
    $username = $_SESSION['username'];
    $data_user = $user->getByUsername($username);
}

$data_keranjang = $transaction->my_cart($data_user['user_id']);


if(isset($_POST['checkout'])){
    $transaction->checkout();
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Checkout</title>
</head>
<body>
<h3>Alamat Pengiriman</h3>
<p><?= $data_user['address'] ?></p>
<p><?= $data_user['phone'] ?></p>

<h3>Pesanan Saya</h3>
<table>
    <thead>
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Gambar</th>
                <th>Harga</th>
                <th>Stok</th>
                <th>Penjual</th>
                <th>Kategori</th>
                <th>Jumlah</th>
            </tr>
            <?php foreach($data_keranjang as $key => $cart) : ?>
                <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $product->getById($cart['product_id'])['name'] ?></td>
                    <td><?= $product->getById($cart['product_id'])['image'] ?></td>
                    <td><?= $product->getById($cart['product_id'])['price'] ?></td>
                    <td><?= $product->getById($cart['product_id'])['stock'] ?></td>
                    <td><?= $product->getSellerById($product->getById($cart['product_id'])['seller_id'])['name'] ?></td>
                    <td><?= $product->getById($cart['product_id'])['category'] ?></td>
                    <td><?= $cart['quantity'] ?></td>
                </tr>
                <?php endforeach ?>
        </thead>
    </table>

    <h3>Rincian Pembayaran</h3>
    <p>Pesanan: <?= $transaction->getTotalCart($data_user['user_id']) ?></p>
    <p>Ongkir: 9000</p>
    <p>Total: <?= $transaction->getTotalCart($data_user['user_id']) + 9000 ?></p>
    
    <form method="POST" action="">
        <input type="hidden" name="customer_id" value="<?= $data_user['user_id'] ?>">
        <input type="submit" name="checkout" value="Checkout">
    </form>
</body>
</html>