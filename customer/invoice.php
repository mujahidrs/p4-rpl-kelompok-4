<?php
session_start();

include_once('restrict.php');
 

include_once('../class/User.php');
include_once('../class/Transaction.php');
include_once('../class/Profile.php');

$invoice_code = $_GET['code'];

$user = new User;
$profile = new Profile;
$company_profile = $profile->index();

if(isset($_SESSION['username'])){
    $username = $_SESSION['username'];
    $data_user = $user->getByUsername($username);
}

$transaction = new Transaction;
$data_transaction = $transaction->getTransactionByInvoiceCode($invoice_code);

$nominal = $transaction->getTotalCheckout($data_user['user_id']) + 9000;

if(isset($_POST['confirm_paid'])){
    $transaction->confirmPaid($invoice_code);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice #<?= $invoice_code ?></title>
</head>
<body>
    <h3>Nominal Transfer</h3>
    <?= $nominal ?>
    <h3>Transfer ke</h3>
    <p><?= $company_profile['bank_name'] ?> <?= $company_profile['account_number'] ?></p>
    <p>a. n. <?= $company_profile['account_name'] ?></p>

    <form method="POST" action="">
        <input type="hidden" name="invoice_code" value="<?= $invoice_code ?>">
        <input type="submit" name="confirm_paid" value="Konfirmasi Pembayaran">
    </form>
</body>
</html>